/*global Store, Model, View, Controller, $$ */

var timings,timeObj={};
(function () {
	'use strict';

	/**
	 * Sets up a brand new Todo list.
	 *
	 * @param {string} name The name of your new to do list.
	 */
	function Todo(name) {
		this.storage = new app.Store(name);
		this.model = new app.Model(this.storage);
		this.view = new app.View();
		this.controller = new app.Controller(this.model, this.view);
	}

	var todo = new Todo('todos-vanillajs');
	var APIKEY = "1da59a0a3e87249c9d05c6f23bff12eb";

	/**
	 * Finds the model ID of the clicked DOM element
	 *
	 * @param {object} target The starting point in the DOM for it to try to find
	 * the ID of the model.
	 */
	function lookupId(target) {
		var lookup = target;

		while (lookup.nodeName !== 'LI') {
			lookup = lookup.parentNode;
		}

		return lookup.dataset.id;
	}

	// When the enter key is pressed fire the addItem method.
	/*$$('#new-todo').addEventListener('keypress', function (e) {
		todo.controller.addItem(e);
	});

	// A delegation event. Will check what item was clicked whenever you click on any
	// part of a list item.
	$$('#todo-list').addEventListener('click', function (e) {
		var target = e.target;

		// If you click a destroy button
		if (target.className.indexOf('destroy') > -1) {
			todo.controller.removeItem(lookupId(target));
		}

		// If you click the checkmark
		if (target.className.indexOf('toggle') > -1) {
			todo.controller.toggleComplete(lookupId(target), target);
		}

	});

	$$('#todo-list').addEventListener('dblclick', function (e) {
		var target = e.target;

		if (target.nodeName === 'LABEL') {
			todo.controller.editItem(lookupId(target), target);
		}
	});

	$$('#toggle-all').addEventListener('click', function (e) {
		todo.controller.toggleAll(e);
	});

	$$('#clear-completed').addEventListener('click', function () {
		todo.controller.removeCompletedItems();
	});*/

	function appendToList(key,value){
		var list = $$('#todo-list');
		var listItem = document.createElement("li"); 
		listItem.innerHTML = key + " : " + value;
		list.appendChild(listItem);
	}
	function loadNamazTimes(){
		var xhr = new XMLHttpRequest();
		xhr.open("GET", 'http://muslimsalat.com/daily.json?key=' + APIKEY + '', true);
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
			  // WARNING! Might be evaluating an evil script!
			  // var resp = eval("(" + xhr.responseText + ")");
			  var list = $$('#todo-list');
			  console.log("ok");
			  console.log(list);
			  console.log(list.children[0]);
			  list.removeChild(list.children[0]);
			  var resp = JSON.parse(xhr.responseText);
			  timings = resp.items[0];
			  console.log(timings);
			  for(var key in timings){
			  	if((key == "fajr" || key=="dhuhr" || key == "asr" || key == "maghrib" || key =="isha") && timings.hasOwnProperty(key)){
			  		timeObj[key]=  moment(timings[key],'hh:mm a');
		  			appendToList(key,timings[key]);
		  			alarmService.checkAlarm({alarmName:key,prayerTime:timeObj[key]},function(alarmExists,config){
	  					if(alarmExists){
	  						console.log("alarm for " + config.alarmName + " already exists");
	  						alarmService.cancelAlarm(config);
	  						alarmService.createAlarm(config);
	  					}
	  					else{
	  						console.log("alarm for " + key + " does not exist");
	  						alarmService.createAlarm(config);
	  					}
		  			});
			  	}
			  	else{
			  		delete timings[key];
			  	}
			  }

			  console.log(timeObj);
			}
		}
		xhr.send();
	}
	loadNamazTimes();
})();
