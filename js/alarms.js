var alarmService;
(function () {
  'use strict';
   /*var alarmName = 'remindme';
   function checkAlarm(callback) {
     chrome.alarms.getAll(function(alarms) {
       var hasAlarm = alarms.some(function(a) {
         return a.name == alarmName;
       });
       var newLabel;
       if (hasAlarm) {
         newLabel = 'Cancel alarm';
       } else {
         newLabel = 'Activate alarm';
       }
       document.getElementById('toggleAlarm').innerText = newLabel;
       if (callback) callback(hasAlarm);
     })
   }
   function createAlarm() {
     chrome.alarms.create(alarmName, {
       delayInMinutes: 0.1, periodInMinutes: 0.1});
   }
   function cancelAlarm() {
     chrome.alarms.clear(alarmName);
   }
   function doToggleAlarm() {
     checkAlarm( function(hasAlarm) {
       if (hasAlarm) {
         cancelAlarm();
       } else {
         createAlarm();
       }
       checkAlarm();
     });
   }
  $$('#toggleAlarm').addEventListener('click', doToggleAlarm);
  checkAlarm();*/

   function checkAlarm(config,callback) {
     config = (config)? config: {};
     var alarmName = (config.alarmName)? config.alarmName : "Reminder";
     chrome.alarms.getAll(function(alarms) {
       var hasAlarm = alarms.some(function(a) {
         return a.name == alarmName;
       });
       console.log("value of hasAlarm is " + hasAlarm);
       if (callback) callback(hasAlarm,config);
     })
   }
   function createAlarm(config) {
     config = (config)? config: {};
     var alarmName = (config.alarmName)? config.alarmName : "Reminder";
     console.log("config for createAlarm");
     console.log(config);
     console.log("\n");
     var differenceInMinutes = config.prayerTime.diff(moment(),'minutes') + 30; //30 means half an hour after the adhan(for now)
     console.log(differenceInMinutes);
     console.log("differenceInMinutes");
     if(differenceInMinutes>0){
      chrome.alarms.create(alarmName, {
         delayInMinutes: differenceInMinutes, 
         // delayInMinutes: Math.random(), 
         periodInMinutes: 5
       }); 
      console.log("alarm created")
     }
   }
   function cancelAlarm(config) {
     config = (config)? config: {};
     var alarmName = (config.alarmName)? config.alarmName : "Reminder";
     chrome.alarms.clear(alarmName);
     console.log("alarm cancelled");
   }
   /*function doToggleAlarm() {
     checkAlarm( function(hasAlarm) {
       if (hasAlarm) {
         cancelAlarm();
       } else {
         createAlarm();
       }
       checkAlarm();
     });
   }*/
  // $$('#toggleAlarm').addEventListener('click', doToggleAlarm);
  alarmService = {
    checkAlarm:checkAlarm,
    createAlarm:createAlarm,
    cancelAlarm:cancelAlarm
  }
})();