/**
 * Listens for the app launching then creates the window
 *
 * @see http://developer.chrome.com/apps/app.window.html
 */
var dbName = 'todos-vanillajs';
var APIKEY = "1da59a0a3e87249c9d05c6f23bff12eb";
var timings = [];
// var notifSound = new Audio('assets/azan.mp3');
var notifSound = new Audio('assets/chime_notif.mp3');
function launch() {
	chrome.app.runtime.onLaunched.addListener(function() {
	  chrome.app.window.create('index.html', {
	    id: 'main',
	    bounds: { width: 620, height: 500 }
	  });
	});
}

function audioNotification(){
    if(notifSound.paused){
      notifSound.pause();
      notifSound.load();
    }
    notifSound.play();
}

function showNotification(storedData,alarm) {
  /*var openTodos = 0;
  if ( storedData[dbName].todos ) {
    storedData[dbName].todos.forEach(function(todo) {
      if ( !todo.completed ) {
        openTodos++;
      }
    });
  }
  if (openTodos>0) {
    // Now create the notification
    chrome.notifications.create('reminder', {
        type: 'basic',
        iconUrl: 'icon.png',
        title: 'Don\'t forget!',
        message: 'You have '+openTodos+' things to do. Wake up, dude!'
     }, function(notificationId) {});
  }*/
  console.log("showing notification");
  console.log(alarm);
  chrome.notifications.create(alarm.name, {
      type: 'basic',
      iconUrl: 'icon.png',
      title: 'Get Ready Get Ready!',
      message: "Dude! get ready for namaz-e-" + alarm.name
   }, function(notificationId) {
    audioNotification();
   });
}


chrome.app.runtime.onLaunched.addListener(launch);

chrome.alarms.onAlarm.addListener(function( alarm ) {
  // console.log("Got an alarm!", alarm);
  chrome.storage.local.get(dbName, function(storedData){
    showNotification(storedData,alarm);
  });
});


chrome.notifications.onClicked.addListener(function() {
  notifSound.pause();
  notifSound.load();
  // launch();
});
chrome.notifications.onClosed.addListener(function (){
  notifSound.pause();
  notifSound.load();
});